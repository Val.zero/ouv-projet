(** Utilitary methods to work on trees. *)

(** A tree, which can be either a leaf, a node or a compressed node *)
type tree =
| Leaf
| Node of int * tree * tree * int * bool (** Node: label, left, right, size, lock *)
| CNode of int array * tree (** CNode: elements in prefix order, tree of matching structure *)

(**
  Makes a new node with leaves as left and right subtrees.
  @param n the integer label of the root
*)
val make_tree : int -> tree

(** Returns a tree's size. *)
val get_size : tree -> int

(** Returns a tree's label if the tree is a node, None otherwise. *)
val get_label : tree -> int option

(**
  Inserts an element in the tree.
  @param n the element to insert
  @param t the tree in which to insert
  @return the tree with the element added (or unchanged if the element was already in)
*)
val insert : tree -> int -> tree

(**
  Makes a tree containing the elements of a list, inserted in order.
  @param list a list
  @return the root of the new tree, with the first element of the list as label
*)
val tree_from_list : int list -> tree

(**
  Returns the tree's signature, which represents its structure.
  With l as the left subtree and r as the right one, the signature is :
  - empty if the tree is a leaf
  - ("(" signature l ")" signature r) otherwise
  @return a string containing the tree's signature
*)
val signature : tree -> string

(**
  Returns the elements of the tree in prefix order.
  @return a list of the tree's elements.
*)
val prefix : tree -> int array

(**
  Compresses the tree by comparing subtrees structure.
  @param tree a tree
  @return a compressed tree
*)
val compress : tree -> tree

(**
  Looks for an element in the tree.
  @param n the element we are looking for
  @return true if the element is in the tree, false otherwise
*)
val search : int -> tree -> bool