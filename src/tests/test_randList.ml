open RandList;;

let test_gen_permutation () =
  let perm = gen_permutation 8 in
  let () = assert (List.length perm = 8) in
  let () = assert (List.mem 1 perm = true) in
  let () = assert (List.mem 4 perm = true) in
  let () = assert (List.mem 8 perm = true) in
  ()

let test_gen_permutation2 () =
  let perm = gen_permutation2 1 8 in
  let () = assert (List.length perm = 8) in
  let () = assert (List.mem 1 perm = true) in
  let () = assert (List.mem 4 perm = true) in
  let () = assert (List.mem 8 perm = true) in
  ()

let test_suite = [
  test_gen_permutation;
  test_gen_permutation2;
]

let rec run suite =
  match suite with
  | h::t -> (h () ; run t)
  | [] -> true;;

if run test_suite then
  print_string "Test RandList : Ok\n"
else
  print_string "Test RandList : Fail\n"