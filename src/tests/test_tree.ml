open Tree;;

let test_make_tree () =
  let t = make_tree 0 in
  match t with
  | Node (label, left, right, size, _) -> let () = assert (label = 0) in
                                          let () = assert (left = Leaf) in
                                          let () = assert (right = Leaf) in
                                          let () = assert (size = 1) in ()
  | _ -> let () = assert false in ()

let test_get_size () =
  let t = make_tree 0 in
  let () = assert (get_size t = 1) in ()

let test_get_label () =
  let t = make_tree 0 in
  let () = assert (Option.get (get_label t) = 0) in ()

let test_insert () =
  let t = make_tree 1 in
  let t = insert t 0 in
  let t = insert t 2 in
  match t with
  | Node (_, left, right, _, _) -> let leftLabel = Option.get (get_label left) in
                                   let rightLabel = Option.get (get_label right) in
                                   let () = assert (leftLabel = 0) in
                                   let () = assert (rightLabel = 2) in ()
  | _ -> let () = assert false in ()

let test_tree_from_list () =
  let t = tree_from_list [4;2;3;8;1;9;6;7;5] in
  match t with
  | Node (_, l, r, _, _) -> (match l with
                             | Node (_, ll, lr, _, _) -> (match r with
                                                           | Node (_, rl, rr, _, _) -> let () = assert (Option.get (get_label ll) = 1) in
                                                                                       let () = assert (Option.get (get_label lr) = 3) in
                                                                                       let () = assert (Option.get (get_label rl) = 6) in
                                                                                       let () = assert (Option.get (get_label rr) = 9) in ()
                                                           | _ -> let () = assert false in ())
                             | _ -> let () = assert false in ())
  | _ -> let () = assert false in ()

let test_signature () =
  let t = tree_from_list [4;2;3;8;1;9;6;7;5] in
  let () = assert (String.equal (signature t) "((())())((())())()") in ()

let test_prefix () =
  let t = tree_from_list [4;2;3;8;1;9;6;7;5] in
  let () = assert ((prefix t) = [|4;2;1;3;8;6;5;7;9|]) in ()

let rec debug_tree = function
| Leaf -> ()
| Node (n, l, r, _, _) -> print_string ("Node "^(string_of_int n)^"\n"); debug_tree l; debug_tree r
| CNode (_, _) -> print_string "CNode\n"

let test_compress () =
  let is_compressed = function
  | CNode (_, _) -> true
  | _ -> false in
  let t = tree_from_list [4;2;3;8;1;9;6;7;5] in
  let t = compress t in
  match t with
    | Node (_, l, r, _, _) -> (match l with
                               | Node (_, ll, lr, _, _) -> (match r with
                                                             | Node (_, rl, rr, _, _) -> let () = assert (not (is_compressed ll)) in
                                                                                         let () = assert (is_compressed lr) in
                                                                                         let () = assert (is_compressed rl) in
                                                                                         let () = assert (is_compressed rr) in ()
                                                             | _ -> let () = assert false in ())
                               | _ -> let () = assert false in ())
    | _ -> let () = assert false in ()

let test_search () =
  let t = tree_from_list [4;2;3;8;1;9;6;7;5] in
  let ct = compress t in
  let () = assert (search 5 t) in
  let () = assert (not (search 10 t)) in
  let () = assert (search 7 ct) in
  let () = assert (not (search 11 ct)) in ()

let test_suite = [
  test_make_tree;
  test_get_size;
  test_get_label;
  test_insert;
  test_tree_from_list;
  test_signature;
  test_prefix;
  test_compress;
  test_search;
]

let rec run suite =
  match suite with
  | h::t -> (h () ; run t)
  | [] -> true;;

if run test_suite then
  print_string "Test Tree : Ok\n"
else
  print_string "Test Tree : Fail\n"