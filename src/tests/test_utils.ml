open Utils;;
open Result;;

let test_comp () =
  let () = assert (comp 1 3 = Lesser) in
  let () = assert (comp 3 1 = Greater) in
  let () = assert (comp 2 2 = Equal) in
  ()

let test_unwrap () =
  let () = assert (unwrap (Ok 5) = 5) in
  let () = assert (unwrap (Error 5) = 5) in
  ()

let test_contains_ok () = 
  let l1 = [Error(0); Error(0); Error(0); Error(0); Error(0)] in 
  let l2 = [Error(0); Error(0); Ok(1); Error(0); Error(0)] in
  let () = assert (contains_ok l1 = false) in
  let () = assert (contains_ok l2 = true) in
  ()

let test_suite = [
  test_comp;
  test_unwrap;
  test_contains_ok;
]

let rec run suite =
  match suite with
  | h::t -> (h () ; run t)
  | [] -> true;;

if run test_suite then
  print_string "Test Utils : Ok\n"
else
  print_string "Test Utils : Fail\n"