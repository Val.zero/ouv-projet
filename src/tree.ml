open Result;;
open Option;;
open Utils;;

(** A tree, which can be either a leaf, a node or a compressed node *)
type tree =
| Leaf
| Node of int * tree * tree * int * bool (** Node: label, left, right, size, lock *)
| CNode of int array * tree (** CNode: elements in prefix order, tree of matching structure *)

(**
  Makes a new node with leaves as left and right subtrees.
  @param n the integer label of the root
*)
let make_tree n = Node(n, Leaf, Leaf, 1, false)

(** Returns a tree's size. *)
let rec get_size = function
  | Leaf -> 0
  | Node (_, _, _, size, _) -> size
  | CNode (_, t) -> get_size t

(** Returns a tree's label if the tree is a node, None otherwise. *)
let rec get_label = function
  | Node (label, _, _, _, _) -> Some(label)
  | _ -> None

(**
  Inserts an element in the tree.
  @param n the element to insert
  @param t the tree in which to insert
  @return the tree with the element added (or unchanged if the element was already in)
*)
let insert t n =
  let rec insert_aux n t =
      match t with
      | Leaf -> Ok (make_tree n)
      | CNode (_, _) -> Error (t)
      | Node (m, l, r, size, lock) -> if lock then Error (t)
                                      else match comp n m with
                                            | Lesser -> (let tmp = insert_aux n l in
                                                        match tmp with
                                                        | Ok(v) -> Ok (Node(m, v, r, size + 1, lock))
                                                        | Error(v) -> Error (Node(m, v, r, size, lock)))
                                            | Greater -> (let tmp = insert_aux n r in
                                                         match tmp with
                                                         | Ok(v) -> Ok (Node(m, l, v, size + 1, lock))
                                                         | Error(v) -> Error (Node(m, l, v, size, lock)))
                                            | Equal -> Error (t)
  in
  unwrap (insert_aux n t)

(**
  Makes a tree containing the elements of a list, inserted in order.
  @param list a list
  @return the root of the new tree, with the first element of the list as label
*)
let tree_from_list list = List.fold_left insert Leaf list

(**
  Returns the tree's signature, which represents its structure.
  With l as the left subtree and r as the right one, the signature is :
  - empty if the tree is a leaf
  - ("(" signature l ")" signature r) otherwise
  @return a string containing the tree's signature
*)
let rec signature = function
  | Leaf -> ""
  | Node (_, l, r, _, _) -> String.concat "" ["("; signature l; ")"; signature r]
  | CNode (_, t) -> signature t

(**
  Returns the elements of the tree in prefix order.
  @return a list of the tree's elements.
*)
let rec prefix = function
  | Leaf -> [||]
  | Node (m, l, r, _, _) -> Array.concat [[|m|]; prefix l; prefix r]
  | CNode (tab, _) -> tab

(**
  If the tree is a node, sets its lock to true.
  @param tree a tree
  @return the tree
*)
let lock_tree tree =
  match tree with
  | Node (m, l, r, s, _) -> Node (m, l, r, s, true)
  | _ -> tree

(**
  Compresses the tree by comparing subtrees structure.
  @param tree a tree
  @return a compressed tree
*)
let rec compress tree =
  let rec compress_aux tree model =
    match tree with
    | Leaf -> Error(tree)
    | CNode (_, _) -> Ok(tree)
    | Node (label, left, right, size, lock) -> if String.equal (signature tree) (signature model) then
                                                 Ok (CNode (prefix tree, lock_tree model))
                                               else match model with
                                                    | Leaf -> Error(tree)
                                                    | CNode (_, mtree) -> compress_aux tree mtree
                                                    | Node (_, ml, mr, _, _) -> match compress_aux tree ml with
                                                                                | Ok(v) -> Ok(v)
                                                                                | Error(_) -> match compress_aux tree mr with
                                                                                              | Ok(v) -> Ok(v)
                                                                                              | Error(_) -> let newleft = compress_aux left model in
                                                                                                            let newright = compress_aux right model in
                                                                                                            Error (Node (label, unwrap newleft, unwrap newright, size, lock))
  in
  match tree with
  | Leaf -> tree
  | CNode (_, _) -> tree
  | Node (label, left, right, size, lock) -> let newleft = compress left in
                                             let newright = compress_aux right left in
                                             Node (label, newleft, unwrap newright, size, is_ok newright)

(**
  Looks for an element in the tree.
  @param n the element we are looking for
  @return true if the element is in the tree, false otherwise
*)
let rec search n tree =
  let rec search_aux n tab i = function
    | Leaf -> false
    | CNode (_, t) -> search_aux n tab i t
    | Node (_, left, right, size, _) -> match comp n (tab.(i)) with
                                        | Equal -> true
                                        | Lesser -> search_aux n tab (i + 1) left
                                        | Greater -> search_aux n tab (i + (get_size left) + 1) right in
  match tree with
  | Leaf -> false
  | CNode (tab, t) -> search_aux n tab 0 t
  | Node (m, left, right, _, _) -> match comp n m with
                                   | Equal -> true
                                   | Lesser -> search n left
                                   | Greater -> search n right

(*;;print_string "trees are cool\n"*)