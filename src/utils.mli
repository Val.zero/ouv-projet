(** Some useful tools. *)

(** Sum type for comparisons. *)
type cmp = Lesser | Equal | Greater

(** Cmparison function. *)
val comp : int -> int -> cmp

(** A function to get the value out of a Result. *)
val unwrap : ('a, 'a) result -> 'a

(** Returns true if the list contains an ok Result, false otherwise. *)
val contains_ok : ('a, 'b) result list -> bool