open Result;;

(** Sum type for comparisons. *)
type cmp =
| Lesser
| Equal
| Greater

(** Cmparison function. *)
let comp x y =
  if x < y then Lesser
  else if x > y then Greater
  else Equal

(** A function to get the value out of a Result. *)
let unwrap = function
  | Ok (v) -> v
  | Error (v) -> v

(** Returns true if the list contains an ok Result, false otherwise. *)
let rec contains_ok = function
  | [] -> false
  | h::t -> match h with
            | Ok(_) -> true
            | Error(_) -> contains_ok t