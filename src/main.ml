open RandList;;
open Tree;;

let gp = ref false
let nunc = ref false
let nc = ref false
let s = ref 100

let print_title () =
  let () = print_string "==================================================\n" in
  let () = print_string "                  Ocaml program\n" in
  print_string "==================================================\n\n"

let timed_gen_perm n =
  let () = print_string "Using gen_permutation\n" in
  let t = Sys.time() in
  let perm = gen_permutation n in
  let dt = Sys.time() -. t in
  let () = Printf.printf "Permutation generation time: %fs\n\n%!" dt in
  perm

let timed_gen_perm2 n =
  let () = print_string "Using gen_permutation2\n" in
  let t = Sys.time() in
  let perm = gen_permutation2 1 n in
  let dt = Sys.time() -. t in
  let () = Printf.printf "Permutation generation time: %fs\n\n%!" dt in
  perm

let timed_search n tree =
  let t = Sys.time() in
  let _ = search n tree in
  let dt = Sys.time() -. t in
(*  let () = Printf.printf "Searching time: %fs\n%!" dt in*)
  dt

let rec perform_search tree searchlist sum =
  match searchlist with
  | h::t -> perform_search tree t (sum +. timed_search h tree)
  | [] -> sum /. 10.

let timed_compress tree =
    let t = Sys.time() in
    let ct = compress tree in
    let dt = Sys.time() -. t in
    let () = Printf.printf "Compression time: %fs\n%!" dt in
    ct

let sizeof x = Obj.reachable_words (Obj.repr x)

let main =
  let () = print_title () in
  let speclist = [("-g", Arg.Set gp, "Uses gen_permutation instead of gen_perumation2");
                  ("-N", Arg.Set nunc, "Disables execution on uncompressed tree");
                  ("-n", Arg.Set nc, "Disables execution on compressed tree");
                  ("-s", Arg.Set_int s, "Sets the size of the list to build the tree from");] in
  let f x = () in
  let usage_msg = "Makes a tree, compress it, and perform random searches." in
  Arg.parse speclist f usage_msg;
  let searchlist = gen_permutation2 1 10 in
  let () = (if !gp then
            (let list = timed_gen_perm !s in
            let t = tree_from_list list in
            let () = (Printf.printf "Uncompressed tree size: %d mem words\n%!" (sizeof t)) in
            let () = if not(!nunc) then (Printf.printf "Average searching time in uncompressed tree: %fs\n\n%!" (perform_search t searchlist 0.)) in
            if not(!nc) then
              (let ct = timed_compress t in
              let () = (Printf.printf "Compressed tree size: %d mem words\n%!" (sizeof ct)) in
              Printf.printf "Average searching time in compressed tree: %fs\n\n%!" (perform_search ct searchlist 0.)))
          else
            (let list = timed_gen_perm2 !s in
            let t = tree_from_list list in
            let () = (Printf.printf "Uncompressed tree size: %d mem words\n%!" (sizeof t)) in
            let () = if not(!nunc) then (Printf.printf "Average searching time in uncompressed tree: %fs\n\n%!" (perform_search t searchlist 0.)) in
            if not(!nc) then
              (let ct = timed_compress t in
              let () = (Printf.printf "Compressed tree size: %d mem words\n%!" (sizeof ct)) in
              Printf.printf "Average searching time in compressed tree: %fs\n\n%!" (perform_search ct searchlist 0.)))) in
  print_string "End of program.\n"

let () = main