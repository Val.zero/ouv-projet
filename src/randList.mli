(** Methods to generate integer lists in random order. *)

(**
  Generates a random permutation of integers from 1 to n (included).
  @param n an integer
  @return a list containing the n first integers in random order
*)
val gen_permutation : int -> int list

(**
  Generates a random permutation of integers from p to q (included).
  @param p an integer
  @param q an integer
  @return a list containing the integers from p to q in random order
*)
val gen_permutation2 : int -> int -> int list