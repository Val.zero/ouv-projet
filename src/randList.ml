Random.self_init () ;;

let extraction_alea l p =
  let len = List.length l in
  let r = Random.int len in

  let rec res l1 l2 id =
    match l1 with
    | h::t -> if id = 0 then
          (t, h::p)
        else
          let (a, b) = res t p (id - 1) in
          (h::a, b)
    | [] -> (l, p)
  in
  res l p r;;

let gen_permutation n =
  let rec list_l n i =
    if i = 0 then
      [n]
    else
      (n - i)::list_l n (i - 1) in
  let l = list_l n (n-1) in
  let p = [] in

  let rec fill_by_alea l p =
    match l with
    | [] -> p
    | _ -> let (l1, l2) = extraction_alea l p in
        fill_by_alea l1 l2
  in fill_by_alea l p;;


let intercale l1 l2 =
  let len1 = List.length l1 in
  let len2 = List.length l2 in

  let rec intercale_rec l1 l2 len1 len2 res =
    match (l1, l2) with
    | ([], _) -> List.append res l2
    | (_, []) -> List.append res l1
    |(h1::t1, h2::t2) -> let proba = (Float.of_int len1) /. (Float.of_int (len1 + len2)) in
        if (Random.float (Float.of_int 1)) < proba then
          intercale_rec t1 l2 (len1 - 1) len2 (h1::res)
        else
          intercale_rec l1 t2 len1 (len2 - 1) (h2::res)
  in
  intercale_rec l1 l2 len1 len2 [];;


let rec gen_permutation2 p q =
  if (p > q) then
    []
  else if p = q then
    [p]
  else
    intercale (gen_permutation2 p ((p + q) / 2)) (gen_permutation2 ((p + q) / 2 + 1) q)
;;