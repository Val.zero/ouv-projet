#!/bin/sh

cd src
#compile
ocamlc -c utils.mli utils.ml ./tests/test_utils.ml
ocamlc -c randList.mli randList.ml ./tests/test_randList.ml
ocamlc -c tree.mli tree.ml ./tests/test_tree.ml
ocamlc -o test_utils utils.cmo ./tests/test_utils.cmo
ocamlc -o test_randList utils.cmo randList.cmo ./tests/test_randList.cmo
ocamlc -o test_tree utils.cmo tree.cmo ./tests/test_tree.cmo

#run tests
./test_utils
./test_randList
./test_tree

#flush
for elem in test_*
  do rm "$elem"
done
for elem in *.cmo
  do rm "$elem"
done
for elem in *.cmi
  do rm "$elem"
done
for elem in ./tests/*.cmo
  do rm "$elem"
done
for elem in ./tests/*.cmi
  do rm "$elem"
done