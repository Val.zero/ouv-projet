#!/bin/sh

#Flush
[ -d bin ] && rm -r bin

#Compile and run
mkdir bin
cd src
ocamlc -c utils.mli utils.ml
for elem in $*
  do if [ -f "$elem".mli ]
    then ocamlc -c "$elem".mli "$elem".ml
  else ocamlc -c "$elem".ml
  fi
done
cd ..
for elem in ./src/*.cmo
  do mv "$elem" ./bin
done
for elem in ./src/*.cmi
  do mv "$elem" ./bin
done
cd bin
for elem in $*
  do ocamlc -o "$elem" utils.cmo "$elem".cmo
  ./"$elem"
done