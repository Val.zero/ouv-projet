# Projet Ouv

Projet de l'UE Ouverture du master d'Informatique de Sorbonne Université (2020/2021).

## Description

Ce programme est le résultat de notre projet d'Ouverture. Il a pour but de tester l'efficacité d'un algorithme de compression d'arbre pour la recherche. 

Dans le détail : 
* le fichier `randList.ml` contient les fonctions nécessaires à la génération de permutations aléatoires d'entiers
* le fichier `tree.ml` contient tout ce qui concerne la génération, la compression et l'utilisation d'arbres
* le fichier `main.ml` fait usage des deux précédents et de fonctions d'affichage et de mesure pour répondre à notre besoin
* les scripts `compileAndRun_bytecode.sh` et `test.sh` sont utilisables à des fins de tests
* le script `build_opt.sh` permet de construire le programme
* le script `run.sh` lance le programme (après l'avoir construit si nécessaire)

## Utilisation

De manière générale, on lancera run.sh sans paramètre pour un aperçu du fonctionnement du programme. 

Pour des tests plus précis, on pourra utiliser les paramètres suivants : 
* `-g` pour utiliser le premier générateur de permutations au lieu du second
* `-N` pour ne pas exécuter de recherche sur l'arbre non-compressé
* `-n` pour ne pas exécuter de recherche sur l'arbre compressé
* `-s n` avec `n` un entier pour spécifier le nombre d'entiers que doit contenir l'arbre