#!/bin/sh

#Flush
[ -d bin ] && rm -r bin

#Compile and run
mkdir bin
cd src
ocamlopt -c utils.mli utils.ml
ocamlopt -c randList.mli randList.ml
ocamlopt -c tree.mli tree.ml
ocamlopt -c main.ml
cd ..
for elem in ./src/*.cmx
  do mv "$elem" ./bin
done
for elem in ./src/*.cmi
  do mv "$elem" ./bin
done
for elem in ./src/*.o
  do mv "$elem" ./bin
done
cd bin
ocamlopt -o main utils.cmx randList.cmx tree.cmx main.cmx